<?php

namespace Kuai6\Queue;

use Kuai6\Queue\Exception\ConnectionException;
use Kuai6\Queue\Exception\InvalidArgumentException;

/**
 * Class Server
 * @package Kuai6\Queue
 */
class Server implements ServerInterface
{
    /**
     * @var \AMQPConnection
     */
    protected $connection;

    /**
     * @var bool
     */
    protected $isConnected = false;

    /**
     * @var \AMQPChannel
     */
    protected $channel = null;

    /**
     * @var array
     */
    protected $params = [];

    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Connect to AMQP server using connection params
     *
     * @return bool
     * @throws ConnectionException
     */
    public function connect()
    {
        if ($this->isConnected) {
            return true;
        }

        $connection = new \AMQPConnection();
        $connection->setHost($this->params['hostname']);
        $connection->setPort($this->params['port']);
        $connection->setLogin($this->params['username']);
        $connection->setPassword($this->params['password']);
        $connection->setVhost($this->params['vhost']);

        $this->connection = $connection;

        try {
            $connection->connect();
        } catch (\AMQPConnectionException $e) {
            $this->isConnected = false;
            throw new ConnectionException($e->getMessage());
        }
        $this->isConnected = true;
        $this->channel = new \AMQPChannel($this->connection);
        return $this->isConnected;
    }

    /**
     * Return AMQP connection
     * @return \AMQPConnection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Close connection if opened
     */
    public function close()
    {
        if ($this->connection instanceof \AMQPConnection && $this->connection->isConnected()) {
            $this->connection->disconnect();
        }

        $this->isConnected = false;
    }

    /**
     * Declare queue, register its on MQ-server
     *
     * @param Queue $queue
     * @param null $bindExchange
     * @return Queue
     */
    public function declareQueue(Queue $queue, $bindExchange = null)
    {
        $this->checkConnection();

        $options = $queue->getOptions();
        $queue->setServer($this);

        $flags = null;

        if (isset($options['durable']) && $options['durable']) {
            $flags |= AMQP_DURABLE;
        }

        if (isset($options["exclusive"]) && $options["exclusive"]) {
            $flags |= AMQP_EXCLUSIVE;
        }

        if (isset($options["passive"]) && $options["passive"]) {
            $flags |= AMQP_PASSIVE;
        }

        if (isset($options["auto_delete"]) && $options["auto_delete"]) {
            $flags |= AMQP_AUTODELETE;
        }

        $q = new \AMQPQueue($this->channel);
        $q->setName($queue->getName());
        $q->setFlags($flags);
        $q->declareQueue();


        if ($bindExchange != null) {
            $queue->bind($bindExchange);
        }

        return $queue;
    }

    /**
     * Delete queue
     *
     * @param Queue $queue
     * @return bool
     */
    public function deleteQueue(Queue $queue)
    {
        $this->checkConnection();

        $q = new \AMQPQueue($this->channel);
        $q->setName($queue->getName());
        return $q->delete();
    }

    /**
     * Declare exchange
     *
     * @param Exchange $exchange
     * @return Exchange
     */
    public function declareExchange(Exchange $exchange)
    {
        $this->checkConnection();

        $ex = new \AMQPExchange($this->channel);
        $ex->setName($exchange->getName());
        $ex->setType($exchange->getType());
        $ex->setFlags(AMQP_DURABLE);
        $ex->declareExchange();

        return $exchange;
    }

    /**
     * Delete exchange
     *
     * @param Exchange $exchange
     * @return bool
     */
    public function deleteExchange(Exchange $exchange)
    {
        $this->checkConnection();

        $ex = new \AMQPExchange($this->channel);
        return $ex->delete($exchange->getName());
    }

    /**
     * Bind queue to Exchange
     *
     * @param Queue $queue
     * @param $exchange
     * @param string $routingKey
     * @return bool
     */
    public function queueBind(Queue $queue, $exchange, $routingKey = '')
    {
        $this->checkConnection();
        $exchangeName = '';

        if ($exchange instanceof Exchange) {
            $exchangeName = $exchange->getName();
        } elseif (is_string($exchange)) {
            $exchangeName = $exchange;
        }

        $q = new \AMQPQueue($this->channel);
        $q->setName($queue->getName());
        return $q->bind($exchangeName, $routingKey);
    }

    /**
     * Send message object to Queue
     *
     * @param Message $message
     * @param Exchange|string $exchange
     * @param string $routingKey
     * @throws Exception\InvalidArgumentException
     *
     * @return bool
     */
    public function send(Message $message, $exchange, $routingKey = '')
    {
        $this->checkConnection();

        if (!is_string($exchange) && !$exchange instanceof Exchange) {
            throw new InvalidArgumentException("Param 'exchange' must be a string or Exchange.");
        }

        $exchangeName = $exchange instanceof Exchange ? $exchange->getName() : $exchange;

        $data = serialize($message->getData());

        $ex = new \AMQPExchange($this->channel);
        $ex->setName($exchangeName);
        return $ex->publish($data, $routingKey, AMQP_NOPARAM, $message->getProperties());
    }

    /**
     * Get message from queue
     *
     * @param Queue $queue
     * @return Message|null
     */
    public function receive(Queue $queue)
    {
        $this->checkConnection();

        $q = new \AMQPQueue($this->channel);
        $q->setName($queue->getName());
        $msg = $q->get(AMQP_NOPARAM);

        if (!$msg) {
            return null;
        }

        $message = $queue->buildMessage($msg);
        $message->init();
        $message->setServer($this);

        return $message;
    }

    /**
     * Consume messages
     *
     * @param Queue $queue
     * @param $callback
     */
    public function consume(Queue $queue, $callback)
    {
        $this->checkConnection();

        $q = new \AMQPQueue($this->channel);
        $q->setName($queue->getName());
        $q->consume(function (\AMQPEnvelope $msg) use ($callback, $queue) {
            $message = $queue->buildMessage($msg);
            $message->init();
            $message->setServer($this);
            return $callback($message);
        });
    }

    /**
     * Send message acknowledgment
     *
     * @param $deliveryTag
     * @return bool
     */
    public function ack($deliveryTag)
    {
        $this->checkConnection();

        $q = new \AMQPQueue($this->channel);
        return $q->ack($deliveryTag);
    }

    /**
     * Send negative message acknowledgment
     *
     * @param $deliveryTag
     * @param bool $requeue
     * @return bool
     */
    public function nack($deliveryTag, $requeue = false)
    {
        $this->checkConnection();

        $q = new \AMQPQueue($this->channel);

        $flag = AMQP_NOPARAM;

        if ($requeue) {
            $flag = AMQP_REQUEUE;
        }

        return $q->nack($deliveryTag, $flag);
    }

    /**
     * Check for opened connection
     * @throws ConnectionException
     */
    private function checkConnection()
    {
        if (!$this->connection || !$this->connection->isConnected()) {
            $this->connect();
        }
    }
}
