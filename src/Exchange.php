<?php

namespace Kuai6\Queue;

/**
 * Class Exchange
 * @package Kuai6\Queue
 */
class Exchange implements ExchangeInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type = 'direct';

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @param $name
     * @param string $type
     * @param array|null $options
     */
    public function __construct($name, $type = 'direct', array $options = null)
    {
        $this->name = $name;
        $this->type = $type;
        $this->options = $options;
    }

    /**
     * Return exchange name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns exchange type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
