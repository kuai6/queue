<?php

namespace Kuai6\Queue\Exception;

/**
 * Class InvalidArgumentException
 * @package Kuai6\Queue\Exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
