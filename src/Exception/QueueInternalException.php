<?php

namespace Kuai6\Queue\Exception;

/**
 * Class QueueInternalException
 * @package Kuai6\Queue\Exception
 */
class QueueInternalException extends \Exception implements ExceptionInterface
{
}
