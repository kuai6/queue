<?php

namespace Kuai6\Queue\Exception;

/**
 * Class BadConfigException
 * @package Kuai6\Queue\Exception
 */
class BadConfigException extends \Exception implements ExceptionInterface
{
}
