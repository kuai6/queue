<?php

namespace Kuai6\Queue\Exception;

/**
 * Class ConnectionException
 * @package Kuai6\Queue\Exception
 */
class ConnectionException extends \Exception implements ExceptionInterface
{
}
