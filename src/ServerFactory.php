<?php

namespace Kuai6\Queue;

use Kuai6\Queue\Exception\BadConfigException;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ServerFactory
 * @package Kuai6\Queue
 */
class ServerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return Server
     * @throws BadConfigException
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');

        if (!array_key_exists('queue', $config) || !array_key_exists('connection', $config['queue'])) {
            throw new BadConfigException('You mus configure "queue" section in your module config. See SEADME.md in Kuai6\\Queue module');
        }
        return new Server($config['queue']['connection']);
    }
}
