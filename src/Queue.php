<?php

namespace Kuai6\Queue;

use Kuai6\Queue\Exception\QueueInternalException;

/**
 * Class Queue
 * @package Kuai6\Queue
 */
class Queue implements QueueInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $exchange = '';

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var Server
     */
    protected $server;

    /**
     * @param $name
     * @param array|null $options
     */
    public function __construct($name, array $options = null)
    {
        $this->name = $name;

        $defaults = [
            'passive' => false,
            'durable' => false,
            'exclusive' => false,
            'auto_delete' => false,
            'nowait' => false
        ];

        if (!empty($options)) {
            $this->options = array_merge($defaults, $options);
        } else {
            $this->options = $defaults;
        }
    }

    /**
     * Return queue name
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Return options array
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Send message to queue
     *
     * @param Message $message
     * @param string $routingKey
     * @return bool
     */
    public function send(Message $message, $routingKey = '')
    {
        return $this->getServer()->send($message, $this->getExchange(), $routingKey);
    }

    /**
     * Receive messages
     *
     * @return Message|null
     */
    public function receive()
    {
        return $this->getServer()->receive($this);
    }

    /**
     * Bind queue to exchange
     * @param Exchange|string $exchange
     * @param string $routingKey
     */
    public function bind($exchange, $routingKey = '')
    {
        if ($exchange instanceof Exchange) {
            $this->exchange = $exchange->getName();
        } elseif (is_string($exchange)) {
            $this->exchange = $exchange;
        }
        $this->getServer()->queueBind($this, $exchange, $routingKey);
    }

    /**
     * @return string
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * @param Server $server
     * @return $this
     */
    public function setServer(Server $server)
    {
        $this->server = $server;
        return $this;
    }

    /**
     * @return Server
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * Build queue specific message object
     *
     * @param \AMQPEnvelope $msg
     * @return \Kuai6\Queue\Message
     * @throws Exception\QueueInternalException
     */
    public function buildMessage(\AMQPEnvelope $msg)
    {
        $options = $this->getOptions();
        $messageClass = Message::class;

        if (isset($options['messageClassName'])) {
            $messageClass = $options['messageClassName'];
        }

        if (!class_exists($messageClass)) {
            throw new QueueInternalException(sprintf(
                'Message class %s not found.', $messageClass));
        }

        if ($messageClass != Message::class) {
            $parentsClasses = class_parents($messageClass);
            if (!in_array('Kuai6\Queue\Message', $parentsClasses)) {
                throw new QueueInternalException(sprintf(
                    'Message class %s must extends from %s', $messageClass, Message::class));
            }
        }

        $messageObject = new $messageClass($msg);
        return $messageObject;
    }
}
