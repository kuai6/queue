<?php

namespace Kuai6\Queue;

use Kuai6\Queue\Exception\QueueInternalException;

/**
 * Class Message
 * @package Kuai6\Queue
 */
class Message implements  MessageInterface
{
    /**
     * @var string
     */
    const ATTRIBUTE_NAME_TYPE = 'type';

    /**
     * @var string
     */
    const ATTRIBUTE_NAME_APP_ID = 'app_id';

    /**
     * @var string
     */
    const ATTRIBUTE_NAME_USER_ID = 'user_id';

    /**
     * @var string
     */
    const ATTRIBUTE_NAME_PRIORITY = 'priority';

    /**
     * @var string
     */
    const ATTRIBUTE_NAME_REPLY_TO = 'reply_to';

    /**
     * @var string
     */
    const ATTRIBUTE_NAME_TIMESTAMP = 'timestamp';

    /**
     * @var string
     */
    const ATTRIBUTE_NAME_EXPIRATION = 'expiration';

    /**
     * @var string
     */
    const ATTRIBUTE_NAME_MESSAGE_ID = 'message_id';

    /**
     * @var string
     */
    const ATTRIBUTE_NAME_CONTENT_TYPE = 'content_type';

    /**
     * @var string
     */
    const ATTRIBUTE_NAME_DELIVERY_MODE = 'delivery_mode';

    /**
     * @var string
     */
    const ATTRIBUTE_NAME_CONTENT_ENCODING = 'content_encoding';

    /**
     * @var array
     */
    protected $attributes = [
        self::ATTRIBUTE_NAME_TYPE,
        self::ATTRIBUTE_NAME_APP_ID,
        self::ATTRIBUTE_NAME_USER_ID,
        self::ATTRIBUTE_NAME_PRIORITY,
        self::ATTRIBUTE_NAME_REPLY_TO,
        self::ATTRIBUTE_NAME_TIMESTAMP,
        self::ATTRIBUTE_NAME_EXPIRATION,
        self::ATTRIBUTE_NAME_MESSAGE_ID,
        self::ATTRIBUTE_NAME_CONTENT_TYPE,
        self::ATTRIBUTE_NAME_DELIVERY_MODE,
        self::ATTRIBUTE_NAME_CONTENT_ENCODING,
    ];

    const DELIVERY_MODE_PERSISTENT = 2;

    const DELIVERY_MODE_NON_PERSISTENT = 1;

    protected $data = [];

    protected $properties = [];

    protected $messageId = null;

    /**
     * @var Server
     */
    protected $server;

    /**
     * @var \AMQPEnvelope
     */
    protected $msg = null;

    /**
     * @param null $data
     * @param array|null $properties
     * @throws QueueInternalException
     */
    public function __construct($data = null, array $properties = null)
    {
        $this->messageId = strtoupper(uniqid());
        // default properties
        $defaults = [
            self::ATTRIBUTE_NAME_DELIVERY_MODE => self::DELIVERY_MODE_PERSISTENT,
            self::ATTRIBUTE_NAME_CONTENT_TYPE => 'text/plain',
            self::ATTRIBUTE_NAME_MESSAGE_ID => $this->messageId,
            self::ATTRIBUTE_NAME_TIMESTAMP => time()
        ];

        $this->properties = $defaults;

        if (!empty($properties)) {
            $this->properties = array_merge($this->properties, $properties);
        }

        // setting message data
        if (!is_null($data)) {
            if ($data instanceof \AMQPEnvelope) {
                $this->setAMQPMessage($data);
            } elseif (is_array($data)) {
                $this->setData($data);
            } else {
                throw new QueueInternalException('Incorrect type of data attribute');
            }
        }
    }

    /**
     *
     */
    public function init()
    {
    }

    /**
     * Set AMQP Message object
     *
     * @param \AMQPEnvelope $msg
     * @return $this
     */
    protected function setAMQPMessage(\AMQPEnvelope $msg)
    {
        $this->msg = $msg;
        // init properties
        foreach ($this->attributes as $attr) {
            $method = 'get' . implode(array_map('ucfirst', explode('_', $attr)));
            if (method_exists($msg, $method)) {
                $value = $msg->$method();
                if ($value !== null) {
                    $this->properties[$attr] = $value;
                }
            }
        }
        $this->data = unserialize($msg->getBody());
        return $this;
    }

    /**
     * Set server instance
     * @param Server $server
     * @return $this
     */
    public function setServer(Server $server)
    {
        $this->server = $server;
        return $this;
    }

    /**
     * @return Server
     * @throws QueueInternalException
     */
    public function getServer()
    {
        if (!$this->server) {
            throw new QueueInternalException(sprintf('%s instance not found', Server::class));
        }
        return $this->server;
    }

    /**
     * Confirm message (ack)
     *
     * @return bool
     * @throws QueueInternalException
     */
    public function confirm()
    {
        return $this->getServer()->ack($this->msg->getDeliveryTag());
    }

    /**
     * Reject message (nack)
     *
     * @param bool|false $requeue
     * @return bool
     * @throws QueueInternalException
     */
    public function reject($requeue = false)
    {
        return $this->getServer()->nack($this->msg->getDeliveryTag(), $requeue);
    }

    /**
     * @return string
     */
    public function getDeliveryTag()
    {
        return $this->msg->getDeliveryTag();
    }

    /**
     * Return message options
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getProperty($name)
    {
        if (array_key_exists($name, $this->properties)) {
            return $this->properties[$name];
        }
        return null;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        return null;
    }

    /**
     * Set data to message
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Return message data
     * @return array|null
     */
    public function getData()
    {
        return $this->data;
    }
}
