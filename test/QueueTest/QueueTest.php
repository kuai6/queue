<?php


namespace QueueTest;

use Kuai6\Queue\Exception\QueueInternalException;
use Kuai6\Queue\Message;
use Kuai6\Queue\Queue;
use Zend\ServiceManager\ServiceManager;

/**
 * Class QueueTest
 * @package QueueTest
 */
class QueueTest extends AbstractTestCase
{
    public function testBuildMessage()
    {
        // test not exist Message Class
        $queue = new Queue('phpunit.testing.queue', [
            'messageClassName' => '\Not\Exist\Message\Class'
        ]);
        try {
            $queue->buildMessage(new \AMQPEnvelope());
        } catch (\Exception $e) {
            static::assertInstanceOf(QueueInternalException::class, $e);
        }

        $mockMessage = $this->getMockBuilder(Message::class)
            ->setMockClassName('TestMockMessage')
            ->getMock();
        $queue = new Queue('phpunit.testing.queue', [
            'messageClassName' => get_class($mockMessage)
        ]);

        //test extends
        static::assertInstanceOf(Message::class, $queue->buildMessage(new \AMQPEnvelope()));

        //test wrong extends
        $mockMessage = $this->getMockBuilder(ServiceManager::class)
            ->getMock();
        $queue = new Queue('phpunit.testing.queue', [
            'messageClassName' => get_class($mockMessage)
        ]);
        try {
            $queue->buildMessage(new \AMQPEnvelope());
        } catch (\Exception $e) {
            static::assertInstanceOf(QueueInternalException::class, $e);
        }
    }
}
