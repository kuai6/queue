<?php

namespace QueueTest;

use Kuai6\Queue\Exception\ConnectionException;
use Kuai6\Queue\Exception\InvalidArgumentException;
use Kuai6\Queue\Exchange;
use Kuai6\Queue\Message;
use Kuai6\Queue\Queue;
use Kuai6\Queue\Server;
use Kuai6\Queue\ServerFactory;


/**
 * Class ServerTest
 * @package QueueTest
 */
class ServerTest extends AbstractTestCase
{
    /** @var  Server */
    protected $server;


    public function testConnection()
    {
        //test false connection
        $server = new Server([
            'hostname' => '127.0.0.256', //wrong address
            'port'      => 5672,
            'username'  => 'guest',
            'password'  => 'guest',
            'vhost'     => '/'
        ]);
        try {
            $server->connect();
        } catch (\Exception $e) {
            static::assertInstanceOf(ConnectionException::class, $e);
        }

        $server = $this->getServer();
        $result = $server->connect();
        static::assertTrue($result);
        //check connection
        static::assertTrue($server->connect());

        /** @var $connection \AMQPConnection */
        $connection = $server->getConnection();
        static::assertInstanceOf(\AMQPConnection::class, $connection);
        static::assertTrue($connection->isConnected());
    }

    public function testQueue()
    {
        $server = $this->getServer();

        // standart queue
        $queue1 = new Queue('phpunit.testing.queue1');
        static::assertInstanceOf(Queue::class, $server->declareQueue($queue1));
        $server->deleteQueue($queue1);

        // durable queue
        $queue2 = new Queue('phpunit.testing.queue2', ['durable' => true]);
        static::assertInstanceOf(Queue::class, $server->declareQueue($queue2));
        $server->deleteQueue($queue2);

        // auto-delete queue
        $queue3 = new Queue('phpunit.testing.queue3', ['durable' => true, 'auto_delete' => true]);
        static::assertInstanceOf(Queue::class, $server->declareQueue($queue3));
        $server->deleteQueue($queue3);

        // all options
        $queue4 = new Queue('phpunit.testing.queue4', [
            //'passive' => true,
            'durable' => true,
            'exclusive' => true,
            'auto_delete' => true,
            'nowait' => true
        ]);
        static::assertInstanceOf(Queue::class, $server->declareQueue($queue4));
        $queue4 = new Queue('phpunit.testing.queue4', [
            'passive' => true,
        ]);
        static::assertInstanceOf(Queue::class, $server->declareQueue($queue4));
        $server->deleteQueue($queue4);
    }

    public function testDeclareExchange()
    {
        $server = $this->getServer();
        $exchange = $server->declareExchange(new Exchange('phpunit.testing.exchange1'));
        static::assertInstanceOf(Exchange::class, $exchange);
        $server->deleteExchange($exchange);
    }

    public function testQueueBind()
    {
        $server = $this->getServer();

        $queue1 = new Queue('phpunit.testing.queue1');
        $server->declareQueue($queue1);

        $queue2 = new Queue('phpunit.testing.queue2');
        $server->declareQueue($queue2);

        $exchange = new Exchange('phpunit.testing.exchange1');
        $server->declareExchange($exchange);

        $queue3 = new Queue('phpunit.testing.queue3');
        static::assertInstanceOf(Queue::class, $server->declareQueue($queue3, $exchange));
        $server->deleteQueue($queue3);

        static::assertTrue($server->queueBind($queue1, $exchange));
        static::assertTrue($server->queueBind($queue1, 'phpunit.testing.exchange1'));
        static::assertTrue($server->queueBind($queue2, $exchange, 'test.routing'));

        $server->deleteQueue($queue1);
        $server->deleteQueue($queue2);
        $server->deleteExchange($exchange);
    }


    public function testSend()
    {
        $server = $this->getServer();

        $exchange = new Exchange('phpunit.testing.exchange1');
        $server->declareExchange($exchange);

        $message = new Message();
        $message->setData('data');
        try {
            $server->send($message, null);
        } catch (\Exception $e) {
            static::assertInstanceOf(InvalidArgumentException::class, $e);
        }

        $message = new Message();
        $message->setData('data');
        static::assertTrue($server->send($message, $exchange));

        $message = new Message();
        $message->setData('data');
        static::assertTrue($server->send($message, 'phpunit.testing.exchange1'));

        $message = new Message();
        $message->setData('data');
        static::assertTrue($server->send($message, 'phpunit.testing.exchange1', 'test.routing'));

        $queue = new Queue('phpunit.testing.queue');
        static::assertInstanceOf(Queue::class, $server->declareQueue($queue));
        $queue->bind($exchange);
        $queue->bind($exchange->getName());


        $message = new Message();
        $message->setData('TestDataMessageContent');
        static::assertTrue($queue->send($message));

        $message = $queue->receive();
        static::assertInstanceOf(Message::class, $message);
        static::assertEquals('TestDataMessageContent', $message->getData());

        $server->deleteQueue($queue);
        $server->deleteExchange($exchange);
    }


    public function testReceive()
    {
        $server = $this->getServer();
        $exchange = new Exchange('phpunit.testing.exchange1');
        $server->declareExchange($exchange);

        $queue = new Queue('phpunit.testing.queue');
        static::assertInstanceOf(Queue::class, $server->declareQueue($queue));

        $server->queueBind($queue, $exchange);

        $message = new Message();
        $message->setData('TestDataMessageContent');
        $server->send($message, $exchange);

        $message = $server->receive($queue);
        static::assertInstanceOf(Message::class, $message);
        static::assertEquals('TestDataMessageContent', $message->getData());
        static::assertNull($server->receive($queue)); //no messages in queue

        $server->deleteQueue($queue);
        $server->deleteExchange($exchange);
    }

    public function testConsume()
    {
        $server = $this->getServer();
        $exchange = new Exchange('phpunit.testing.exchange1');
        $server->declareExchange($exchange);

        $queue = new Queue('phpunit.testing.queue');
        static::assertInstanceOf(Queue::class, $server->declareQueue($queue));

        $server->queueBind($queue, $exchange);

        $testMessageContent = 'TestDataMessageContent';

        $message = new Message(null, [
            Message::ATTRIBUTE_NAME_DELIVERY_MODE => Message::DELIVERY_MODE_NON_PERSISTENT,

        ]);
        $message->setData($testMessageContent);
        $server->send($message, $exchange);

        $server->consume($queue, function ($message) use ($testMessageContent) {
            static::assertInstanceOf(Message::class, $message);
            static::assertEquals($testMessageContent, $message->getData());
            return false; //callback must return false!
        });

        $server->deleteQueue($queue);
        $server->deleteExchange($exchange);
    }

    public function testAck()
    {
        $server = $this->getServer();
        $exchange = new Exchange('phpunit.testing.exchange');
        $server->declareExchange($exchange);

        $queue = new Queue('phpunit.testing.queue');
        static::assertInstanceOf(Queue::class, $server->declareQueue($queue));
        $server->queueBind($queue, $exchange, 'test.routing');

        $message = new Message();
        $message->setData('TestDataMessageContent');
        $message->setServer($server);
        $server->send($message, $exchange, 'test.routing');

        $message = $server->receive($queue);
        static::assertNotEmpty($message->getDeliveryTag());
        static::assertInstanceOf(Message::class, $message);
        static::assertEquals('TestDataMessageContent', $message->getData());
        static::assertTrue($message->confirm());

        $server->deleteQueue($queue);
        $server->deleteExchange($exchange);
    }

    public function testNack()
    {
        $server = $this->getServer();
        $exchange = new Exchange('phpunit.testing.exchange');
        $server->declareExchange($exchange);

        $queue = new Queue('phpunit.testing.queue');
        static::assertInstanceOf(Queue::class, $server->declareQueue($queue));
        $server->queueBind($queue, $exchange, 'test.routing');

        $message = new Message();
        $message->setData('TestDataMessageContent');
        $message->setServer($server);
        $server->send($message, $exchange, 'test.routing');

        $message = $server->receive($queue);
        static::assertInstanceOf(Message::class, $message);
        static::assertEquals('TestDataMessageContent', $message->getData());
        static::assertTrue($message->reject(true));

        $message = $server->receive($queue);
        static::assertInstanceOf(Message::class, $message);
        static::assertEquals('TestDataMessageContent', $message->getData());
        static::assertTrue($message->reject(false));

        $server->deleteQueue($queue);
        $server->deleteExchange($exchange);
    }


    /**
     * Tears down the fixture, for example, close a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        $this->getServer()->close();
        parent::tearDown();
    }


    /**
     * @return Server
     */
    public function getServer()
    {
        if ($this->server === null) {
            $this->server = $this->serviceManager->get(ServerFactory::class);
            static::assertInstanceOf(Server::class, $this->server);
        }
        return $this->server;
    }

    /**
     * @param Server $server
     * @return ServerTest
     */
    public function setServer($server)
    {
        $this->server = $server;
        return $this;
    }
}
