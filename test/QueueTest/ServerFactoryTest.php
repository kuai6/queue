<?php

namespace QueueTest;

use Kuai6\Queue\Exception\BadConfigException;
use Kuai6\Queue\ServerFactory;
use Zend\ServiceManager\Config;
use Zend\ServiceManager\ServiceManager;

/**
 * Class ServerFactoryTest
 * @package QueueTest
 */
class ServerFactoryTest extends AbstractTestCase
{
    public function testCreateService()
    {
        $config = $this->config;
        $factory = new ServerFactory();

        unset($config['queue']['connection']);
        $serviceManager = new ServiceManager(new Config($config));
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('config', $config);

        try {
            $factory->createService($serviceManager);
        } catch (\Exception $e) {
            static::assertInstanceOf(BadConfigException::class, $e);
        }

        unset($config['queue']);
        $serviceManager = new ServiceManager(new Config($config));
        $serviceManager->setAllowOverride(true);
        $serviceManager->setService('config', $config);

        try {
            $factory->createService($serviceManager);
        } catch (\Exception $e) {
            static::assertInstanceOf(BadConfigException::class, $e);
        }
    }
}
