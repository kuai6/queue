<?php


namespace QueueTest;

use Kuai6\Queue\Exception\QueueInternalException;
use Kuai6\Queue\Message;
use Kuai6\Queue\Server;

/**
 * Class MessageTest
 * @package QueueTest
 */
class MessageTest extends AbstractTestCase
{
    public function testCreateMessage()
    {
        try {
            $message = new Message(true);
        } catch (\Exception $e) {
            static::assertInstanceOf(QueueInternalException::class, $e);
        }

        $message = new Message([
            'someDataKey' => 'someDataValue'
        ], [
            'somePropertyKey' => 'somePropertyValue'
        ]);

        $data = $message->getData();
        static::assertTrue(is_array($data));
        static::assertArrayHasKey('someDataKey', $data);
        static::assertEquals('someDataValue', $data['someDataKey']);
        static::assertEquals('someDataValue', $message->someDataKey);
        static::assertNull($message->notExistentAttribute);
        $message->someDataKey = 'someDifferentDataValue';
        static::assertEquals('someDifferentDataValue',  $message->someDataKey);

        $properties = $message->getProperties();
        static::assertTrue(is_array($properties));
        static::assertArrayHasKey('somePropertyKey', $properties);
        static::assertEquals('somePropertyValue', $properties['somePropertyKey']);
        static::assertEquals('somePropertyValue', $message->getProperty('somePropertyKey'));
        static::assertNull($message->getProperty('nonExistentProperty'));
    }

    public function testGetServer()
    {
        $message = new Message();
        try {
            $message->getServer();
        } catch (\Exception $e) {
            static::assertInstanceOf(QueueInternalException::class, $e);
        }

        $mockServer = $this->getMockBuilder(Server::class)
            ->disableOriginalConstructor()
            ->getMock();
        $message->setServer($mockServer);
        static::assertInstanceOf(get_class($mockServer), $message->getServer());
    }
}
