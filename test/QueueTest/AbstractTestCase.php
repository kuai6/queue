<?php


namespace QueueTest;

use Zend\ServiceManager\Config;
use Zend\ServiceManager\ServiceManager;

/**
 * Class AbstractTestCase
 * @package QueueTest
 */
class AbstractTestCase extends \PHPUnit_Framework_TestCase
{
    /** @var  array */
    protected $config;

    /** @var  ServiceManager */
    protected $serviceManager;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->config = include __DIR__ .'/../../config/module.config.php';
        $this->serviceManager = new ServiceManager(new Config($this->config['service_manager']));
        $this->serviceManager->setAllowOverride(true);
        $this->serviceManager->setService('config', $this->config);
        parent::setUp();
    }

    /**
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * @param ServiceManager $serviceManager
     * @return AbstractTestCase
     */
    public function setServiceManager($serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }
}
