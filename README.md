Компонент работы с очередями
===================================================

Описание
------------
    Компонент работы со структурой данных "очередь" через расширение функционала AMQP протокола.


Зависимости
------------
    php
    ext-amqp
    zendframework/zend-servicemanager
    zendframework/zend-loader

Установка
------------
composer.json

    "require": {
        "kuai6/queue": "master"
    }

application.config.php

    return array(
        ...
        '\Kuai6\\Queue',
    );

