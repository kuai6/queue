<?php

namespace Kuai6\Queue;

return [
    'queue' => [
        'connection' => [
            'hostname'  => 'localhost',
            'port'      => 5672,
            'username'  => 'guest',
            'password'  => 'guest',
            'vhost'     => '/'
        ],
    ],
    'service_manager' => [
        'factories' => [
            ServerFactory::class => ServerFactory::class
        ],
    ]
];
